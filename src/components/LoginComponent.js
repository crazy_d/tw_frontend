import React from "react";
import {
    Redirect,
} from 'react-router-dom';
import Config from "../config";
import protect from "../utils/protect";
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'
import request from 'superagent';

export default class LoginComponent extends React.Component {
    state = {
        redirectToAdmin: false,
        username: '',
        password: ''
    };

    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleRestorePassword = this.handleRestorePassword.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        request.post(Config.endpoint + '/auth')
            .send({username: this.state.username, password: this.state.password})
            .then(res => {
                if (!res.body.success) {
                    confirmAlert({
                        message: res.message,
                        title: '',
                        cancelLabel: 'Close',
                        confirmLabel: ''
                    });
                } else {
                    protect.authenticate(res.body.token);
                    this.setState({redirectToAdmin: true})
                }
            });

        return false;
    }

    handleChange(event) {
        const data = {};
        data[event.target.name] = event.target.value;
        this.setState(data);
    }

    handleRestorePassword() {
        request.post(Config.endpoint + '/restore').send({
            username: this.state.username
        }).then(res => {
            if (!res.body.success) {
                confirmAlert({
                    message: res.body.message,
                    title: '',
                    cancelLabel: 'Close',
                    confirmLabel: ''
                });
            } else {
                protect.authenticate(res.body.token);
                this.setState({redirectToAdmin: true})
            }
        });
    }

    render() {
        if (this.state.redirectToAdmin) {
            return (
                <Redirect to="/admin"/>
            )
        }

        return (
            <div>
                <form onSubmit={this.handleSubmit} className="form-signin">
                    <h1>Sign in</h1>
                    <div className="form-group"><input className="form-control" name="username" placeholder="Username"
                                                       type="text"
                                                       onChange={this.handleChange}/></div>
                    <div className="form-group"><input className="form-control" name="password" placeholder="Password"
                                                       type="password"
                                                       onChange={this.handleChange}/></div>
                    <div className="form-group"><a className="link" onClick={this.handleRestorePassword}>Restore
                        password</a></div>
                    <div className="form-group">
                        <button className="btn btn-lg btn-primary btn-block" type="submit"
                                disabled={!this.state.username || !this.state.password}>Sign in
                        </button>
                    </div>
                </form>
            </div>
        )
    }
}