import React from 'react';
import protect from '../utils/protect';
import Config from '../config';


export default class FilePreviewComponent extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
        this.handleShowPreview = this.handleShowPreview.bind(this);
    }

    handleClick() {
        this.props.unmountMe(this.props.file.id);
    }

    handleShowPreview() {
        this.props.showPreview(this.props.file.id);
    }

    isImage(name) {
        return ['jpg', 'jpeg', 'png', 'gif'].indexOf(name.split('.').pop()) !== -1;
    }

    render() {
        const file = this.props.file;
        const path = Config.endpoint + '/protected/files/' + file.file_name + '/' + file.original_name + '?token=' + protect.token;
        return <tr className="file-preview">
            <td className="img">
                {this.isImage(file.original_name) ? <img onClick={this.handleShowPreview} src={path} alt={file.original_name}/> : ''}
            </td>
            <td><a href={path} download className="download">{file.original_name}</a></td>
            <td>{file.filesize}</td>
            <td className="text-right">
                <span className="icon-remove" aria-hidden="true" onClick={this.handleClick}>&nbsp;</span>
            </td>
        </tr>
    }
};
