import React from "react";
import {
    Redirect,
} from 'react-router-dom';
import Config from "../config";
import protect from "../utils/protect";
import Recaptcha from "recaptcha-react";
import request from 'superagent';

export default class UploadComponent extends React.Component {
    state = {
        file: null,
        captcha: null,
        redirectAdmin: false
    };

    constructor(props) {
        super(props);
        this.onFormSubmit = this.onFormSubmit.bind(this);
        this.onChange = this.onChange.bind(this);
        this.fileUpload = this.fileUpload.bind(this);
        this.onChangeCaptcha = this.onChangeCaptcha.bind(this);
    }

    onFormSubmit(e) {
        e.preventDefault();
        this.fileUpload(this.state.file).then(() => {
            this.setState({redirectAdmin: true});
        })
    }

    onChange(e) {
        this.setState({file: e.target.files[0]})
    }

    onChangeCaptcha(token) {
        this.setState({captcha: token});
    }

    fileUpload(file) {
        const url = Config.endpoint + '/protected/files/upload';
        const formData = new FormData();
        formData.append('file', file);
        formData.append('g-recaptcha-response', this.state.captcha);
        return request.post(url).send(formData).set('x-access-token', protect.token);
    }

    render() {
        return (
            this.state.redirectAdmin ?
                <Redirect to="/admin"/>
                :
                <form onSubmit={this.onFormSubmit}>
                    <h1>File Upload</h1>
                    <div className="form-group"><input type="file" onChange={this.onChange}/></div>
                    <div className="form-group"><Recaptcha siteKey="6LdPkkQUAAAAAOmgo-eCksZeD57yd6fxJoQhlxbi"
                                                           onChange={this.onChangeCaptcha}/></div>
                    <div className="form-group">
                        <button className="btn btn-primary" type="submit" disabled={!this.state.captcha}>Upload</button>
                    </div>
                </form>
        )
    }
}