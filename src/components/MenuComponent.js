import React from 'react'
import {
    Link,
} from 'react-router-dom';

import protect from "../utils/protect";

const MenuComponent = () => (
    protect.isAuthenticated ? (
        <nav>
            <ul className="nav nav-pills float-right">
                <li className="nav-item"><Link to="/upload" className="nav-link">Upload</Link></li>
                <li className="nav-item"><Link to="/admin" className="nav-link">List files</Link></li>
                <li className="nav-item"><Link to="/logout" className="nav-link">Logout</Link></li>
            </ul>
        </nav>
    ) : (
        <nav>
            <ul className="nav nav-pills float-right">
                <li className="nav-item"><Link to="/login" className="nav-link">Sign in</Link></li>
            </ul>
        </nav>
    )
);

export default MenuComponent;