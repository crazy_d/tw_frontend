import React from 'react';
import FilePreviewComponent from './FilePreviewComponent';
import request from 'superagent';
import protect from '../utils/protect';
import Config from '../config';
import Lightbox from 'react-image-lightbox';
import {confirmAlert} from 'react-confirm-alert';
import 'react-confirm-alert/src/react-confirm-alert.css'

export default class FileListComponent extends React.Component {
    state = {
        files: [],
        photoIndex: 0,
        isLightboxOpen: false,
    };

    constructor(props) {
        super(props);
        this.handleChildUnmount = this.handleChildUnmount.bind(this);
        this.handleShowPreview = this.handleShowPreview.bind(this);
    }

    isImage(name) {
        return ['jpg', 'jpeg', 'png', 'gif'].indexOf(name.split('.').pop()) !== -1;
    }

    componentWillMount() {
        request.get(Config.endpoint + '/protected/files').set('x-access-token', protect.token)
            .then((res) => {
                return JSON.parse(res.text);
            })
            .then((response) => {
                this.setState({
                    files: response.files
                });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    handleChildUnmount(id) {
        confirmAlert({
            message: 'Are you sure to do this?',
            title: '',
            confirmLabel: 'Confirm',
            cancelLabel: 'Cancel',
            onConfirm: () => {
                request.post(Config.endpoint + '/protected/files/delete').type('form').send({
                    id: id
                }).set('x-access-token', protect.token).then((res) => {
                    return JSON.parse(res.text);
                }).then(() => {
                    this.setState({
                        files: this.state.files.filter(file => {
                            return file.id !== id;
                        })
                    });
                }).catch((error) => {
                    console.log(error);
                });
            },
        });
    }

    handleShowPreview(id) {
        const index = this.state.files.findIndex(file => {
            return this.isImage(file.original_name) && file.id === id;
        });
        if (index !== -1) {
            this.setState({photoIndex: index, isLightboxOpen: true})
        }
    }


    render() {
        const {photoIndex, isLightboxOpen} = this.state;
        const images = this.state.files.filter(file => {
            return this.isImage(file.original_name);
        }).map(function (file) {
            return Config.endpoint + '/protected/files/' + file.file_name + '/' + file.original_name + '?token=' + protect.token;
        });

        return this.state.files.length ? <div>
            {isLightboxOpen && (
                <Lightbox
                    mainSrc={images[photoIndex]}
                    nextSrc={images[(photoIndex + 1) % images.length]}
                    prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                    onCloseRequest={() => this.setState({isLightboxOpen: false})}
                    onMovePrevRequest={() =>
                        this.setState({
                            photoIndex: (photoIndex + images.length - 1) % images.length,
                        })
                    }
                    onMoveNextRequest={() =>
                        this.setState({
                            photoIndex: (photoIndex + 1) % images.length,
                        })
                    }
                />
            )}
            <table className="table">
                <thead>
                <tr>
                    <th scope="col">Preview</th>
                    <th scope="col">Filename</th>
                    <th scope="col">Filesize</th>
                    <th scope="col">&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.files.map(file => {
                        return (
                            <FilePreviewComponent showPreview={this.handleShowPreview}
                                                  unmountMe={this.handleChildUnmount}
                                                  file={file} key={file.id}/>
                        );
                    })
                }
                </tbody>
            </table>
        </div> : 'Files not found';
    }
}