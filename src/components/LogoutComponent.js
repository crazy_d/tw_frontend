import React from "react";
import {
    Redirect,
} from 'react-router-dom';
import protect from "../utils/protect";

const LogoutComponent = () => {
    protect.logout();
    return <Redirect to='/login'/>
};

export default LogoutComponent;