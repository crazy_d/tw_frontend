import React from 'react'
import {
    BrowserRouter as Router,
    Route,
    Redirect,
} from 'react-router-dom';

import FileListComponent from './components/FileListComponent';
import MenuComponent from './components/MenuComponent';
import UploadComponent from './components/UploadComponent';
import LogoutComponent from './components/LogoutComponent';
import LoginComponent from './components/LoginComponent';

import protect from './utils/protect';

import './App.css';

if (sessionStorage.getItem('token')) {
    protect.authenticate(sessionStorage.getItem('token'));
}

const App = () => (
    <Router>
        <div>
            <div className="container">
                <div className="header clearfix">
                    <MenuComponent/>
                </div>
                <div>
                    <Route path="/login" component={LoginComponent}/>
                    <PrivateRoute path="/upload" component={UploadComponent}/>
                    <PrivateRoute path="/admin" component={FileListComponent}/>
                    <PrivateRoute path="/logout" component={LogoutComponent}/>
                </div>
            </div>
        </div>
    </Router>
);

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        protect.isAuthenticated ? (
            <Component {...props}/>
        ) : (
            <Redirect to={{
                pathname: '/login',
                state: {from: props.location}
            }}/>
        )
    )}/>
);


export default App;