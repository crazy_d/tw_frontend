const protect = {
    isAuthenticated: false,
    token: false,
    authenticate(token) {
        this.isAuthenticated = true;
        this.token = token;
        sessionStorage.setItem('token', token);
    },
    logout() {
        this.isAuthenticated = false;
        this.token = false;
        sessionStorage.removeItem('token');
    }
};

export default protect;